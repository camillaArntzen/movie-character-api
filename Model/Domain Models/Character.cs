﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Model.Domain_Models
{
    public class Character
    {
        //Pk
        public int Id { get; set; }

        //Fields
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }

        //Relationships 
        public ICollection<Movie> Movies { get; set; }

    }
}
