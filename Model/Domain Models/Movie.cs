﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Model.Domain_Models
{
    public class Movie
    {
        //PK
        public int Id { get; set; }

        //Fields
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }

        [Required]
        [MaxLength(4)]
        public string ReleaseYear { get; set; }

        [Required]
        [MaxLength(50)]
        public string Director { get; set; }

        [Required]
        [MaxLength(2000)]
        public string Picture { get; set; }

        [Required]
        [MaxLength(2000)]
        public string Trailer { get; set; }
        
        //Relationships 
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }

    }
}
