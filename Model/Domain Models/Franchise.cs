﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Model.Domain_Models
{
    public class Franchise
    {
        //Pk
        public int Id { get; set; }

        //Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }

        //Relationships 
        public ICollection<Movie> Movies { get; set; }
    }
}
