﻿using MovieCharactersAPI.Model.DataTransferObjects;
using MovieCharactersAPI.Model.Domain_Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Model.Profiles
{
    public class CharacterProfile : Profile
    {

        public CharacterProfile()
        {
            CreateMap<Character, CharacterDTO>().ReverseMap();
        }
    }
}
