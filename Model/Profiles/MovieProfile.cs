﻿using AutoMapper;
using MovieCharactersAPI.Model.DataTransferObjects;
using MovieCharactersAPI.Model.Domain_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Model.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>().ReverseMap();
            CreateMap<Movie, MovieEntryDTO>().ReverseMap();
        }
    }
}
