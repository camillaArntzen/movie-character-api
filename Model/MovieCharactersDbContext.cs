﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Model.Domain_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Model
{
    public class MovieCharactersDbContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieCharactersDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seeding
            // Movies

            modelBuilder.Entity<Movie>().HasData( new Movie
            {
                Id = 1,
                Title = "Avengers",
                Genre = "Action",
                ReleaseYear = "2012",
                Director = "Joss Whedon",
                Picture= "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer= "https://www.youtube.com/watch?v=eOrNdBpGMv8&ab_channel=MarvelEntertainment",
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                Title = "Avengers Endgame",
                Genre = "Action",
                ReleaseYear = "2019",
                Director = "Russo Brothers",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=TcMBFSGVi1c&ab_channel=MarvelEntertainment",
                FranchiseId = 1
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                Title = "LOTR: Followship of the ring",
                Genre = "Action Adventure",
                ReleaseYear = "2001",
                Director = "Peter Jackson",
                Picture = "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4&ab_channel=Movieclips",
                FranchiseId = 2
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 4,
                Title = "LOTR: The Two Towers",
                Genre = "Action Adventure",
                ReleaseYear = "2002",
                Director = "Peter Jackson",
                Picture = "https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=LbfMDwc4azU&ab_channel=MovieclipsClassicTrailers",
                FranchiseId = 2
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 5,
                Title = "HP and the Sorcerer's Stone",
                Genre = "Fantasy Adventure",
                ReleaseYear = "2001",
                Director = "Chris Columbus",
                Picture = "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo&ab_channel=MovieclipsClassicTrailers",
                FranchiseId = 3
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 6,
                Title = "HP and the Chamber of Secrets",
                Genre = "Fantasy Adventure",
                ReleaseYear = "2002",
                Director = "Chris Columbus",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTcxODgwMDkxNV5BMl5BanBnXkFtZTYwMDk2MDg3._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=1bq0qff4iF8&ab_channel=MovieclipsClassicTrailers",
                FranchiseId = 3
            });

            // Franchises
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 1,
                Name = "MCU",
                Description = "Heroes and villans battle it in classic tales of good vs evil"
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 2,
                Name = "HP Universe",
                Description = "Wizzards, sorcerers and gomps all part of the same world. Some good, some bad"
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 3,
                Name = "Lord of the Rings",
                Description = "The fantastical creatures of middle earth"
            });

            //Characters
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 1,
                FullName = "Peter Parker",
                Alias = "Spiderman",
                Gender = "male",
                Picture = "https://i.imgur.com/xV41WEb.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 2,
                FullName = "Natasha Romanoff",
                Alias = "Black Widow",
                Gender = "female",
                Picture = "https://i.imgur.com/ysJ051W.jpeg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 3,
                FullName = "Tony Stark",
                Alias = "Ironman",
                Gender = "male",
                Picture = "https://i.imgur.com/K8yMR2Q.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 4,
                FullName = "Aragorn",
                Alias = "Strider",
                Gender = "male",
                Picture = "https://i.imgur.com/A2iLbUh.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 5,
                FullName = "Arwen",
                Gender = "female",
                Picture = "https://i.imgur.com/L969M3L.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 6,
                FullName = "Gimli",
                Alias = "of Gloin",
                Gender = "male",
                Picture = "https://i.imgur.com/U6BroBC.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 7,
                FullName = "Harry Potter",
                Gender = "male",
                Picture = "https://i.imgur.com/90sxwzu.jpeg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 8,
                FullName = "Hermione Granger",
                Gender = "female",
                Picture = "https://i.imgur.com/NZl8kBH.jpeg"
            });
            
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 9,
                FullName = "Ron Weasley",
                Gender = "male",
                Picture = "https://i.imgur.com/clPPpMh.jpeg"
            });

        }
    }
}
