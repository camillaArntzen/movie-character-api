﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DataTransferObjects;
using MovieCharactersAPI.Model.Domain_Models;

namespace MovieCharactersAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Movies
        /// <summary>
        /// Get all Movies
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMovies()
        {            
            return _mapper.Map<List<MovieDTO>>(await _context.Movies.ToListAsync());
        }

        // GET: api/Movies/5
        /// <summary>
        /// Get specific Movie by Id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            var movie = _mapper.Map<MovieDTO>(await _context.Movies.FindAsync(id));

            if (movie == null)
            {
                return NotFound();
            }
            return movie;
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update specific Movie by Id
        /// </summary>

        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEntryDTO movieDTO)
        {

            Movie movie = new Movie()
            {
                Id = id,
                Title = movieDTO.Title,
                Genre = movieDTO.Genre,
                ReleaseYear = movieDTO.ReleaseYear,
                Director = movieDTO.Director,
                Picture = movieDTO.Picture,
                Trailer = movieDTO.Trailer,
                FranchiseId = movieDTO.FranchiseId,
            };



            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Post new Movie 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Update /Characters to Movies
        ///     {
        ///         "Title": "Captain America",
        ///         "Genre": "Action Adventure",
        ///         "ReleaseYear": "2014",
        ///         "Director": "Johan Grubelorf"
        ///         "Picture": "link"
        ///         "Trailer" : "link"
        ///         "FranchiseId" : 2
        ///     }
        /// </remarks>
        [HttpPost]
        public async Task<ActionResult<MovieEntryDTO>> PostMovie(MovieEntryDTO movieDTO)
        {
            
            var movie = _mapper.Map<Movie>(movieDTO);           
            _context.Movies.Add(movie);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Delete specific Movie by Id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        
        // GET: api/Movies/{id}/Characters
        /// <summary>
        /// Get all Characters in a specific Movie
        /// </summary>
        [HttpGet("{id}/Characters")]
        public ActionResult<IEnumerable<CharacterDTO>> GetMovieCharacters(int id)
        {
            Movie movie = _context.Movies.Include(c => c.Characters).Where(m => m.Id == id).FirstOrDefault();
            List<CharacterDTO> movieCharacters = new List<CharacterDTO>();

            foreach (Character c in movie.Characters) {
                movieCharacters.Add(_mapper.Map<CharacterDTO>(c));
            }

            return movieCharacters;
        }


        // POST: api/Movies/{id}/Characters/{characterId}
        /// <summary>
        /// Add Character to Movie
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Movies to Character
        ///     {
        ///         1,2,3
        ///     }
        /// </remarks>
        [HttpPost("{id}/Characters/{characterId}")]
        public async Task<IActionResult> PostMovieCharacter(int id, int characterId)
        {
            Movie movie = await _context.Movies.Include(c => c.Characters).SingleAsync(c => c.Id == id);
            Character character = await _context.Characters.FindAsync(characterId);       
            if (!movie.Characters.Contains(character)) movie.Characters.Add(character);

            _context.Entry(movie).State = EntityState.Modified;
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                } else if (!CharacterExists(id))
                {
                    return NotFound();
                } 
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        // DELETE: api/v1/Movies/{id}/Characters/{characterId}
        /// <summary>
        /// Remove character from Movie
        /// </summary>
        [HttpDelete("{id}/Characters/{characterId}")]
        public async Task<IActionResult> RemoveCharacterMovie(int characterId, int id)
        {
            Character character = _context.Characters.Include(c => c.Movies).Where(c => c.Id == characterId).Single();
            if (character == null)
            {
                return NotFound();
            }

            character.Movies.Remove(character.Movies.Single(m => m.Id == id));

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(characterId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
