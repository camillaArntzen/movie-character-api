﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.DTOs;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DataTransferObjects;
using MovieCharactersAPI.Model.Domain_Models;

namespace MovieCharactersAPI.Controllers
{
    //Franchise-controller
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;


        public FranchisesController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all franchises in db
        /// </summary>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchise()
        {
            return _mapper.Map<List<FranchiseDTO>>(await _context.Franchises.ToListAsync());
        }

        /// <summary>
        /// Returns a franchise with a given id
        /// </summary>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            FranchiseDTO franchiseDTO = _mapper.Map<FranchiseDTO>(await _context.Franchises.FindAsync(id));
            if (franchiseDTO == null)
            
            {
                return NotFound();
            }
            
            return franchiseDTO;

        }


        /// <summary>
        /// Updates a franchise with a given id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Update /Franchises
        ///     {
        ///         "name": "Star Wars",
        ///         "description": "A long time ago in a galaxy far, far away",
        ///     }
        /// </remarks>

        // PUT: api/Franchises/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseDTO franchiseDTO)
        {
            if (id != franchiseDTO.Id)
            {
                return BadRequest();
            }

            Franchise franchise = new Franchise()
            {
                Id = id,
                Name = franchiseDTO.Name,
                Description = franchiseDTO.Description
            };

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        /// <summary>
        /// Adds a new franchise to the db
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Franchises
        ///     {
        ///         "name": "James Bond",
        ///         "description": "Movies about some random british agent",
        ///     }
        /// </remarks>
        // POST: api/Franchises
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseDTO franchiseDTO)
        {
            Franchise franchise = new Franchise()
            {
                Name = franchiseDTO.Name,
                Description = franchiseDTO.Description
            };
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            franchiseDTO.Id = franchise.Id;
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }


        /// <summary>
        /// Deletes a franchise by id
        /// </summary>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }


        //Helper method - Checks if franchise exists
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }


        /// <summary>
        /// Returns movies in a franchise
        /// </summary>
        // GET: api/Franchises/{id}/movies
        [HttpGet("{id}/Movies")]
        public ActionResult<IEnumerable<MovieDTO>> GetFranchiseMovies(int id)
        {
            Franchise franchise = _context.Franchises.Include(f => f.Movies).Where(f => f.Id == id).Single();
            List<MovieDTO> franchiseMovies = _mapper.Map<List<MovieDTO>>(franchise.Movies);
            return franchiseMovies;

        }

        /// <summary>
        /// Returns characters in a franchise
        /// </summary>
        // GET: api/franchises/id/characters
        [HttpGet("{id}/Characters")]
        public ActionResult<IEnumerable<CharacterDTO>> GetFranchiseCharacters(int id)
        {

            Movie movie = _context.Movies.Include(c => c.Characters).Where(m => m.FranchiseId == id).FirstOrDefault();
            List<CharacterDTO> franchiseCharacters = new List<CharacterDTO>();

            foreach (Character c in movie.Characters)
            {
                franchiseCharacters.Add(_mapper.Map<CharacterDTO>(c));
            }

            return franchiseCharacters;

        }

    }
    }
