﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DataTransferObjects;
using MovieCharactersAPI.Model.Domain_Models;


namespace MovieCharactersAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharactersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        // GET: api/Characters
        /// <summary>
        /// Get all Characters
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterDTO>>(await _context.Characters.ToListAsync());
        }


        // GET: api/Characters/5
        /// <summary>
        /// Get specific character by Id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> GetCharacter(int id)
        {
            var character = _mapper.Map<CharacterDTO>(await _context.Characters.FindAsync(id));

            if (character == null)
            {
                return NotFound();
            }

            return character;
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Update existing Character
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Update /Characters to Movies
        ///     {
        ///         "fullName": "Bruce Banner",
        ///         "alias": "Hulk",
        ///         "gender": "male",
        ///         "picture": "link"
        ///     }
        /// </remarks>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEntryDTO charDTO)
        {

            Character character = new Character()
            {
                Id = id,
                FullName = charDTO.FullName,
                Alias = charDTO.Alias,
                Gender = charDTO.Gender,
                Picture = charDTO.Picture

            };

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754

        /// <summary>
        /// Create new Character
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Characters
        ///     {
        ///         "id": 10,
        ///         "fullName": "Bruce Banner",
        ///         "alias": "Hulk",
        ///         "gender": "male",
        ///         "picture": "link"
        ///     }
        /// </remarks>
        [HttpPost]
        public async Task<ActionResult<CharacterDTO>> PostCharacter(CharacterDTO charDTO)
        {
            var character = _mapper.Map<Character>(charDTO);
            _context.Characters.Add(character);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Delete specific Character by Id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }

        //MyCode
        //Get movies for character
        /// <summary>
        /// Get all Movies for which a Character is part of
        /// </summary>
        [HttpGet("{id}/movies")]
        public async Task <ActionResult<IEnumerable<MovieDTO>>> GetMovieForChar(int id)
        {
            Character character = await _context.Characters.Include(c => c.Movies).Where(c => c.Id == id).FirstOrDefaultAsync();
            List<MovieDTO> movies = new List<MovieDTO>();
            if (character == null)
            {
                return NotFound();
            }

            foreach (Movie movie in character.Movies)
            {
                movies.Add(_mapper.Map<MovieDTO>(movie));
            }
            return movies;
        }

        // Addd movie for character
        /// <summary>
        /// Add Movie to Character
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Characters to Movies
        ///     {
        ///         1,2,3
        ///     }
        /// </remarks>
        [HttpPost("{id}/movies")]
        public async Task<IActionResult> AddMovieToChar(int id, List<int> movies )
        {
            Character character = await _context.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.Id == id);
            if (character == null)
            {
                return NotFound();
            }
            foreach(int movieId in movies)
            {
                if(character.Movies.FirstOrDefault(c => c.Id == movieId) == null)
                {
                    Movie movie = await _context.Movies.FindAsync(movieId);
                    if(movie != null)
                    {
                        character.Movies.Add(movie);
                    }
                }
            }
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
