# Movie Character API 🎬
The eighth assignment for the Experis Academy Noroff upskill course.

## Features
This project is a .NET CORE Rest API that can store information about franchises, movies and characters.

## Setup
<ul>
<li> Clone the repository</li>
<li> Open the project in Visual Studio </li>
<li> Set the Data source to your DB server name in appsettings.json</li>
<li> Run migrations using the Package Manager Console</li>
<li> ▶ Click on the green arrow if you're a clicky boi or run it by pressing<b> Ctrl+F5 </b></li>
</ul>

## API endpoints
The following endpoints are provided with this API: <br/>
<b>Base url:</b> https://localhost:44370/api/


### Characters
<b>GET: /Characters </b><br/>
Get all characters

<b>POST: /Characters </b><br/>
Create new character

<b>GET: /Characters/{id} </b><br/>
Get specific character by id

<b>PUT: /Characters/{id} </b><br/>
Update existing character

<b>DELETE: /Characters/{id} </b><br/>
Delete specific character by id

<b>GET: /Characters/{id}/movies </b><br/>
Get all Movies for which a character is part of

<b>POST: /Characters/{id}/movies </b><br/>
Add movie to character

### Franchises
<b>GET: /Franchises </b><br/>
Get all franchises

<b>POST: /Franchises </b><br/>
Add a new franchise

<b>GET: /Franchises/{id} </b><br/>
Get a specific franchise by id

<b>PUT: /Franchises/{id} </b><br/>
Update existing franchise

<b>DELETE: /Franchises/{id} </b><br/>
Delete specific franchise by id

<b>GET: /Franchises/{id}/movies </b><br/>
Gets all movies in a franchise

<b>GET: /Franchises/{id}/Characters </b><br/>
Gets all characters in a franchise

### Movies
<b>GET: /Movies </b><br/>
Get all movies

<b>POST: /Movies </b><br/>
Add a new movie

<b>GET: /Movies/{id} </b><br/>
Get specific movie by id

<b>PUT: /Movies/{id} </b><br/>
Update specific movie by id

<b>DELETE: /Movies/{id} </b><br/>
Delete specific movie by id

<b>GET: /Movies/{id}/Characters </b><br/>
Get all characters in a movie

<b>POST: /Movies/{id}/Characters </b><br/>
Add character to movie

<b>DELETE: /Movies/{id}/Characters/{characterId} </b><br/>
Remove character from movie

## Authors
<ul>
<li> Jakob Ramstad</li>
<li> Espen Wiik</li>
<li> Camilla Arntzen</li>
</ul>

## Assignment requirements
<p> &#9989; Swagger Documentation</p>

#### Movies
<p> &#9989; Genereic CRUD for movies</p>
<p> &#9989; Get all characters in  a movie</p>

#### Characters
<p> &#9989; Generic CRUD for characters</p>

#### Franchise
<p> &#9989; Generic CRUD for franchises</p>
<p> &#9989; Get all movies in a franchise</p>
<p> &#9989; Get all characters in a franchise</p>



