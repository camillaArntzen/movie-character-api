﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MovieCharactersAPI.Model;

namespace MovieCharactersAPI.Migrations
{
    [DbContext(typeof(MovieCharactersDbContext))]
    [Migration("20210308190550_MovieCharacter")]
    partial class MovieCharacter
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.3")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.Property<int>("CharactersId")
                        .HasColumnType("int");

                    b.Property<int>("MoviesId")
                        .HasColumnType("int");

                    b.HasKey("CharactersId", "MoviesId");

                    b.HasIndex("MoviesId");

                    b.ToTable("CharacterMovie");
                });

            modelBuilder.Entity("MovieCharactersAPI.Model.Domain_Models.Character", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Alias")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Gender")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Picture")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Characters");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Alias = "Spiderman",
                            FullName = "Peter Parker",
                            Gender = "male",
                            Picture = "https://i.imgur.com/xV41WEb.jpg"
                        },
                        new
                        {
                            Id = 2,
                            Alias = "Black Widow",
                            FullName = "Natasha Romanoff",
                            Gender = "female",
                            Picture = "https://i.imgur.com/ysJ051W.jpeg"
                        },
                        new
                        {
                            Id = 3,
                            Alias = "Ironman",
                            FullName = "Tony Stark",
                            Gender = "male",
                            Picture = "https://i.imgur.com/K8yMR2Q.jpg"
                        },
                        new
                        {
                            Id = 4,
                            Alias = "Strider",
                            FullName = "Aragorn",
                            Gender = "male",
                            Picture = "https://i.imgur.com/A2iLbUh.jpg"
                        },
                        new
                        {
                            Id = 5,
                            FullName = "Arwen",
                            Gender = "female",
                            Picture = "https://i.imgur.com/L969M3L.jpg"
                        },
                        new
                        {
                            Id = 6,
                            Alias = "of Gloin",
                            FullName = "Gimli",
                            Gender = "male",
                            Picture = "https://i.imgur.com/U6BroBC.jpg"
                        },
                        new
                        {
                            Id = 7,
                            FullName = "Harry Potter",
                            Gender = "male",
                            Picture = "https://i.imgur.com/90sxwzu.jpeg"
                        },
                        new
                        {
                            Id = 8,
                            FullName = "Hermione Granger",
                            Gender = "female",
                            Picture = "https://i.imgur.com/NZl8kBH.jpeg"
                        },
                        new
                        {
                            Id = 9,
                            FullName = "Ron Weasley",
                            Gender = "male",
                            Picture = "https://i.imgur.com/clPPpMh.jpeg"
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Model.Domain_Models.Franchise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasMaxLength(255)
                        .HasColumnType("nvarchar(255)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("Franchises");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "Heroes and villans battle it in classic tales of good vs evil",
                            Name = "MCU"
                        },
                        new
                        {
                            Id = 2,
                            Description = "Wizzards, sorcerers and gomps all part of the same world. Some good, some bad",
                            Name = "HP Universe"
                        },
                        new
                        {
                            Id = 3,
                            Description = "The fantastical creatures of middle earth",
                            Name = "Lord of the Rings"
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Model.Domain_Models.Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Director")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<int>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genre")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Picture")
                        .IsRequired()
                        .HasMaxLength(2000)
                        .HasColumnType("nvarchar(2000)");

                    b.Property<string>("ReleaseYear")
                        .IsRequired()
                        .HasMaxLength(4)
                        .HasColumnType("nvarchar(4)");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Trailer")
                        .IsRequired()
                        .HasMaxLength(2000)
                        .HasColumnType("nvarchar(2000)");

                    b.HasKey("Id");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movies");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Director = "Joss Whedon",
                            FranchiseId = 1,
                            Genre = "Action",
                            Picture = "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",
                            ReleaseYear = "2012",
                            Title = "Avengers",
                            Trailer = "https://www.youtube.com/watch?v=eOrNdBpGMv8&ab_channel=MarvelEntertainment"
                        },
                        new
                        {
                            Id = 2,
                            Director = "Russo Brothers",
                            FranchiseId = 1,
                            Genre = "Action",
                            Picture = "https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_UX182_CR0,0,182,268_AL_.jpg",
                            ReleaseYear = "2019",
                            Title = "Avengers Endgame",
                            Trailer = "https://www.youtube.com/watch?v=TcMBFSGVi1c&ab_channel=MarvelEntertainment"
                        },
                        new
                        {
                            Id = 3,
                            Director = "Peter Jackson",
                            FranchiseId = 2,
                            Genre = "Action Adventure",
                            Picture = "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_UX182_CR0,0,182,268_AL_.jpg",
                            ReleaseYear = "2001",
                            Title = "LOTR: Followship of the ring",
                            Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4&ab_channel=Movieclips"
                        },
                        new
                        {
                            Id = 4,
                            Director = "Peter Jackson",
                            FranchiseId = 2,
                            Genre = "Action Adventure",
                            Picture = "https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg",
                            ReleaseYear = "2002",
                            Title = "LOTR: The Two Towers",
                            Trailer = "https://www.youtube.com/watch?v=LbfMDwc4azU&ab_channel=MovieclipsClassicTrailers"
                        },
                        new
                        {
                            Id = 5,
                            Director = "Chris Columbus",
                            FranchiseId = 3,
                            Genre = "Fantasy Adventure",
                            Picture = "https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_UX182_CR0,0,182,268_AL_.jpg",
                            ReleaseYear = "2001",
                            Title = "HP and the Sorcerer's Stone",
                            Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo&ab_channel=MovieclipsClassicTrailers"
                        },
                        new
                        {
                            Id = 6,
                            Director = "Chris Columbus",
                            FranchiseId = 3,
                            Genre = "Fantasy Adventure",
                            Picture = "https://m.media-amazon.com/images/M/MV5BMTcxODgwMDkxNV5BMl5BanBnXkFtZTYwMDk2MDg3._V1_UX182_CR0,0,182,268_AL_.jpg",
                            ReleaseYear = "2002",
                            Title = "HP and the Chamber of Secrets",
                            Trailer = "https://www.youtube.com/watch?v=1bq0qff4iF8&ab_channel=MovieclipsClassicTrailers"
                        });
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.HasOne("MovieCharactersAPI.Model.Domain_Models.Character", null)
                        .WithMany()
                        .HasForeignKey("CharactersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieCharactersAPI.Model.Domain_Models.Movie", null)
                        .WithMany()
                        .HasForeignKey("MoviesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MovieCharactersAPI.Model.Domain_Models.Movie", b =>
                {
                    b.HasOne("MovieCharactersAPI.Model.Domain_Models.Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Franchise");
                });

            modelBuilder.Entity("MovieCharactersAPI.Model.Domain_Models.Franchise", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
